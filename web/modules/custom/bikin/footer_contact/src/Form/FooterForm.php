<?php


namespace Drupal\footer_contact\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides  Socials media form.
 */
class FooterForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'resume_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['footer.form.setting'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('footer.form.setting');
    $form['Developers'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Developers'),
      '#description' => $this->t( 'Please Seperate Email Using Virgule '),
      '#default_value' => $config->get('Developers'),
    ];

    $form['social network link'] = array(
      '#type' => 'details',
      '#title' => $this
        ->t('social network link')
    );
    $form['social network link']['facebook_link'] = [
      '#type' => 'url',
      '#title' => $this->t('facebook link'),
      '#default_value' => $config->get('facebook_link')
    ];
    $form['social network link']['twitter'] = [
      '#type' => 'url',
      '#title' => $this->t('twitter link'),
      '#default_value' => $config->get('twitter')
    ];
    $form['social network link']['google_plus'] = [
      '#type' => 'url',
      '#title' => $this->t('google plus link'),
      '#default_value' => $config->get('google_plus')
    ];
    $form['social network link']['linkedin_link'] = [
      '#type' => 'url',
      '#title' => $this->t('linkedin link'),
      '#default_value' => $config->get('linkedin_link')
    ];
    $form['social network link']['youtube_link'] = [
      '#type' => 'url',
      '#title' => $this->t('youtube'),
      '#default_value' => $config->get('youtube_link')
    ];
    $form['social network link']['drible_link'] = [
      '#type' => 'url',
      '#title' => $this->t('drible linl'),
      '#default_value' => $config->get('drible_link')
    ];
    $form['social network link']['pinterest_link'] = [
      '#type' => 'url',
      '#title' => $this->t('pinterestlink'),
      '#default_value' => $config->get('pinterest_link')
    ];
    $form['social network link']['textfooter'] = [
      '#type' => 'textfield',
      '#title' => $this->t('text footer'),
      '#default_value' => $config->get('textfooter')
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable('footer.form.setting')
      ->set('facebook_link', $form_state->getValue('facebook_link'))
      ->set('twitter', $form_state->getValue('twitter'))
      ->set('google_plus', $form_state->getValue('google_plus'))
      ->set('linkedin_link', $form_state->getValue('linkedin_link'))
      ->set('youtube_link', $form_state->getValue('youtube_link'))
      ->set('drible_link', $form_state->getValue('drible_link'))
      ->set('pinterest_link', $form_state->getValue('pinterest_link'))
      ->set('textfooter', $form_state->getValue('textfooter'))
      ->save();
  }
}
